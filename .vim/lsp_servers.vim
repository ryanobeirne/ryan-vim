let g:lsp_auto_enable = 1
let g:lsp_signs_enabled = 1         " enable diagnostic signs / we use ALE for now
let g:lsp_diagnostics_echo_cursor = 1 " enable echo under cursor when in normal mode
let g:lsp_signs_error = {'text': '✖'}
let g:lsp_signs_warning = {'text': '~'}
let g:lsp_signs_hint = {'text': '?'}
let g:lsp_signs_information = {'text': '!!'}
let g:lsp_log_verbose = 1
let g:lsp_log_file = expand('~/.vim/vim-lsp.log')

" Rust
" let g:racer_cmd = "~/.cargo/bin/racer"
" let g:racer_experimental_completer = 1
" set hidden

"if executable('racer')
	"autocmd User asyncomplete_setup call asyncomplete#register_source(
		"\ asyncomplete#sources#racer#get_source_options())
"endif

" if executable('rls')
    " au User lsp_setup call lsp#register_server({
        " \ 'name': 'rls',
        " \ 'cmd': {server_info->['rustup', 'run', 'stable', 'rls']},
        " \ 'whitelist': ['rust'],
        " \ })
" endif

" if executable('rust-analyzer')
  " au User lsp_setup call lsp#register_server({
        " \   'name': 'Rust Language Server',
        " \   'cmd': {server_info->['rust-analyzer']},
        " \   'whitelist': ['rust'],
        " \ })
" endif

if executable('rust-analyzer')
  au User lsp_setup call lsp#register_server({
        \   'name': 'rust-analyzer',
        \   'cmd': {server_info->['rust-analyzer']},
        \   'whitelist': ['rust'],
        \   'initialization_options': {
        \     'cargo': {
        \       'loadOutDirsFromCheck': v:true,
        \        'allFeatures': v:true,
        \        'autoreload': v:true,
        \     },
        \     'procMacro': {
        \       'enable': v:true,
        \     },
        \   },
        \ })
endif

if executable('eslint')
	au User lsp_setup call lsp#register_server({
		\ 'name': 'eslint',
		\ 'cmd': {server_info->['eslint']},
		\ 'whitelist': ['javascript'],
		\ })
endif

" if executable('tsserver')
	" au User lsp_setup call lsp#register_server({
		" \ 'name': 'tsserver',
		" \ 'cmd': {server_info->['tsserver']},
		" \ 'whitelist': ['typescript'],
		" \ })
" endif

augroup LspGo
  au!
  autocmd User lsp_setup call lsp#register_server({
      \ 'name': 'go-lang',
      \ 'cmd': {server_info->['gopls']},
      \ 'whitelist': ['go'],
      \ })
  autocmd FileType go setlocal omnifunc=lsp#complete
  "autocmd FileType go nmap <buffer> gd <plug>(lsp-definition)
  "autocmd FileType go nmap <buffer> ,n <plug>(lsp-next-error)
  "autocmd FileType go nmap <buffer> ,p <plug>(lsp-previous-error)
augroup END

nmap gd :LspDefinition<CR>
nmap gp :LspPeekDefinition<CR>
nmap gl :LspPeekDeclaration<CR>
nmap ge :LspNextError<CR>
nmap gw :LspNextWarning<CR>

"Register ccls C++ lanuage server.
if executable('ccls')
   au User lsp_setup call lsp#register_server({
      \ 'name': 'ccls',
      \ 'cmd': {server_info->['ccls']},
      \ 'root_uri': {server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'compile_commands.json'))},
      \ 'initialization_options': {'cache': {'directory': '/tmp/ccls/cache' }},
      \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp', 'cc'],
      \ })
endif

" Ruby
if executable('solargraph')
	au User lsp_setup call lsp#register_server({
	  \ 'name': 'solargraph',
	  \ 'cmd': {server_info->[&shell, &shellcmdflag, 'solargraph stdio']},
	  \ 'root_uri':{server_info->lsp#utils#path_to_uri(lsp#utils#find_nearest_parent_file_directory(lsp#utils#get_buffer_path(), 'Gemfile'))},
	  \ 'whitelist': ['ruby', 'eruby'],
	  \ })
endif
