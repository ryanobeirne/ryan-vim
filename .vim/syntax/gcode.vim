" Vim syntax file
" Language: NC 
" Maintainer: Dave Eno <daveeno@gmail.com>
" Last Change: 2014 April 03

" Quit when a (custom syntax file was already loaded
if exists("b:current_syntax")
	finish
endif

syn case ignore

syn  match ncTodo /(TODO)/ contains=ncTodo
syn  match ncComment /;.*$/
syn  keyword ncGCodes G01 G02 G03 G04 G1 G2 G3 G4 G10 G17 G18 G19 G20 G21 G28 G29 G30 G33 G33.1 G38.2 G38.x G40 G41 G41.1 G42 G42.1 G43 G43.1 G49 G53 G54 G55 G56 G57 G58 G59 G59.1 G59.2 G59.3 G61 G61.1 G64 G76 G80 G81 G82 G83 G84 G85 G86 G87 G88 G89 G90 G91 G92 G92.1 G92.2 G92.3 G93 G94 G95 G96 G97 G98 G99
syn  keyword ncMCodes M00 M01 M02 M03 M04 M05 M06 M07 M08 M09 M0 M1 M2 M3 M4 M5 M6 M7 M8 M9 M30 M48 M49 M50 M51 M52 M53 M60 M62 M63 M64 M65 M66

syntax match ncXAxis "\<[X]-\?\d\+\>"
syntax match ncXAxis "\<[X]-\?\.\d\+\>"
syntax match ncXAxis "\<[X]-\?\d\+\."
syntax match ncXAxis "\<[X]-\?\d\+\.\d\+\>"

syntax match ncYAxis "\<[Y]-\?\d\+\>"
syntax match ncYAxis "\<[Y]-\?\.\d\+\>"
syntax match ncYAxis "\<[Y]-\?\d\+\."
syntax match ncYAxis "\<[Y]-\?\d\+\.\d\+\>"

syntax match ncZAxis "\<Z-\?\d\+\>"
syntax match ncZAxis "\<Z-\?\.\d\+\>"
syntax match ncZAxis "\<Z-\?\d\+\."
syntax match ncZAxis "\<Z-\?\d\+\.\d\+\>"

syntax match ncAAxis "\<[ABC]-\?\d\+\>"
syntax match ncAAxis "\<[ABC]-\?\.\d\+\>"
syntax match ncAAxis "\<[ABC]-\?\d\+\."
syntax match ncAAxis "\<[ABC]-\?\d\+\.\d\+\>"

syntax match ncIAxis "\<[IJKR]-\?\d\+\>"
syntax match ncIAxis "\<[IJKR]-\?\.\d\+\>"
syntax match ncIAxis "\<[IJKR]-\?\d\+\."
syntax match ncIAxis "\<[IJKR]-\?\d\+\.\d\+\>"

syntax match ncRapid "\<G\(0\+\)\>"

syntax match ncFeed "\<F-\?\d\+\>"
syntax match ncFeed "\<F-\?\.\d\+\>"
syntax match ncFeed "\<F-\?\d\+\."
syntax match ncFeed "\<F-\?\d\+\.\d\+\>"

syntax match ncExtrude "\<E-\?\d\+\>"
syntax match ncExtrude "\<E-\?\.\d\+\>"
syntax match ncExtrude "\<E-\?\d\+\."
syntax match ncExtrude "\<E-\?\d\+\.\d\+\>"

syntax match ncTool "\<T\d\+\>"

hi def link ncComment Comment
hi def link ncTodo Todo
hi def link ncGCodes Keyword
hi def link ncMCodes Keyword
hi def link ncXAxis Macro
hi def link ncYAxis String
hi def link ncZAxis Function
hi def link ncAAxis VimString

hi def link ncRapid WarningMsg
hi def link ncIAxis Identifier
hi def link ncSpecials SpecialChar
hi def link ncFeed SpecialChar
hi def link ncTool Define

let b:current_syntax = "nc"
