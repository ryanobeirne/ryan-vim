au BufNewFile,BufRead *.plist,*.mxf,*.cxf,*.pxf setf xml
au BufNewFile,BufRead *.env setf dosini
au BufNewFile,BufRead *.ovpn setf openvpn
au BufNewFile,BufRead *.gcode setf gcode
au BufNewFile,BufRead /etc/wireguard/*.conf setf dosini
