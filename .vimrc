"let g:ale_completion_enabled = 1

call plug#begin('~/.vim/plugged')
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'airblade/vim-gitgutter'
Plug 'cespare/vim-toml'
Plug 'chrisbra/csv.vim'
Plug 'itchyny/lightline.vim'
Plug 'jiangmiao/auto-pairs'
Plug 'sheerun/vim-polyglot'
Plug 'joshdick/onedark.vim'
Plug 'kchmck/vim-coffee-script'
Plug 'mhinz/vim-startify'
Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/asyncomplete-file.vim'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'mattn/vim-lsp-settings'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
" Plug 'w0rp/ale'
Plug 'wojtekmach/vim-rename'
Plug 'xuhdev/vim-latex-live-preview', { 'for': 'tex' }
Plug 'Yggdroot/indentLine'
Plug 'csexton/trailertrash.vim'
call plug#end()

filetype plugin indent on

set updatetime=100
set number cursorline hlsearch incsearch linebreak
syntax on
set smartindent autoindent tabstop=4 softtabstop=4 shiftwidth=4
set ignorecase smartcase
set clipboard=unnamed
let mapleader="-"
set tabpagemax=100
set mouse=a
set guifont=Source\ Code\ Pro\ Medium\ 12
set undodir=~/.vimundodir
set undofile
set scrolloff=5
set backspace=indent,eol,start
"set relativenumber

let g:indentLine_char_list = ['│', '|', '¦', '┆', '┊']
let g:AutoPairsMultilineClose = 0

cnoreabbrev tabber set tabstop=8 softtabstop=8 shiftwidth=8
cnoreabbrev wsudo w !sudo tee '%'
cnoreabbrev git Git

set wildmenu
set wildmode=longest:full,full

set grepprg=grep\ -nH\ $*
let g:tex_flavor = "latex"
let g:NERDSpaceDelims = 1

set completeopt+=preview
set completeopt+=menuone
set completeopt+=longest
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<CR>"

autocmd BufNew,BufRead *.sh :inoremap <expr> <Tab> pumvisible() ? "\<C-x><C-f>" : "\<Tab>"
autocmd BufNew,BufRead *.sh :inoremap <expr> <S-Tab> pumvisible() ? "\<C-x><C-f>" : "\<S-Tab>"

" refresh the completion list
imap <C-space> <Plug>(asyncomplete_force_refresh)
nnoremap <leader><leader> :LspHover<CR>
nnoremap <F2> :LspRename<CR>

" Keybindings
"" Line navigation
inoremap <C-a> <C-o>^
inoremap <C-e> <C-o>$

"" Select All
nnoremap <leader>a ggVG
vnoremap <leader>a <ESC>ggVG
"" Paste from system register
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P
"" Copy to system register
nnoremap <leader>y "+y
nnoremap <leader>Y "+Y
vnoremap <leader>y "+y
vnoremap <leader>Y "+Y
nnoremap Y Vy
"" Toggle GitGutter
nnoremap <leader>ggt :GitGutterToggle<CR>
"" Show/hide whitespace characters
set listchars=eol:$,tab:>-,trail:~,extends:>,precedes:<,space:*
nnoremap <leader>l :set list!<CR>
nnoremap <leader>dt :TrailerTrim<CR>

""Don't be a dummy
nnoremap U :echo "TURN CAPS LOCK OFF, DUMMY!"<CR>

" Enable crontab editing in place
au BufNewFile,BufRead crontab.* set nobackup | set nowritebackup

" Set whitespace for specific extensions
autocmd BufNewFile,BufRead *.ts,*.js,*.rb,*.xml,*.json,*.yml,*.html set tabstop=2 softtabstop=2 shiftwidth=2 expandtab
autocmd BufNewFile,BufRead *.py set tabstop=4 softtabstop=4 shiftwidth=4 noexpandtab

let g:NERDCustomDelimiters = { 'gcode': { 'left': ';' } }

" Filename completions
au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#file#get_source_options({
    \ 'name': 'file',
    \ 'whitelist': ['*'],
    \ 'priority': 10,
    \ 'completor': function('asyncomplete#sources#file#completor')
    \ }))

" Register LSP servers
source ~/.vim/lsp_servers.vim

" Latex
let g:livepreview_previewer = 'okular'

" Last position
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

set laststatus=2
let g:lightline = {
\'colorscheme': 'onedark',
\}
" onedark.vim override: Don't set a background color when running in a terminal;
" just use the terminal's background color
" `gui` is the hex color code used in GUI mode/nvim true-color mode
" `cterm` is the color code used in 256-color mode
" `cterm16` is the color code used in 16-color mode
if (has("autocmd") && !has("gui_running"))
  augroup colorset
    autocmd!
    let s:white = { "gui": "#051421", "cterm": "145", "cterm16" : "7" }
    autocmd ColorScheme * call onedark#set_highlight("Normal", { "fg": s:white }) " `bg` will not be styled since there is no `bg` setting
  augroup END
endif
colorscheme onedark
highlight CursorLine ctermbg=0
highlight Visual ctermbg=0
highlight Search cterm=NONE ctermfg=NONE ctermbg=8
highlight WildMenu ctermfg=8
highlight PmenuSel ctermfg=8

" Trailing Whitespace
highlight UnwantedTrailerTrash guibg=red ctermbg=red
nnoremap <leader>dt :TrailerTrim<CR>

" Toggle highlight.
nnoremap <leader><ESC> :set hlsearch!<CR>

" Sessions
let g:session_dir = '~/.vim/sessions'
exec 'nnoremap <Leader>ss :mks! ' . g:session_dir . '/*.vim<C-D><BS><BS><BS><BS><BS>'
exec 'nnoremap <Leader>sr :so ' . g:session_dir. '/*.vim<C-D><BS><BS><BS><BS><BS>'

" vimdiff highlighting
highlight DiffAdd    cterm=bold ctermfg=NONE ctermbg=22 gui=none guifg=bg guibg=Red
highlight DiffDelete cterm=bold ctermfg=NONE ctermbg=52 gui=none guifg=bg guibg=Red
highlight DiffChange cterm=bold ctermfg=NONE ctermbg=235  gui=none guifg=bg guibg=Red
highlight DiffText   cterm=bold ctermfg=NONE ctermbg=8  gui=none guifg=bg guibg=Red
highlight UnwantedTrailerTrash guibg=red ctermbg=red

packloadall
silent! helptags ALL

" Restart LSP Server
nnoremap <F5> :LspStopServer<CR> :sleep<CR> :call lsp#activate()<CR>
